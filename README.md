### 事前準備

#### 開発用プロファイルの設定

###### 環境変数で設定する方法
アプリ起動時もしくはPCの環境変数に下記を追加する

`MICRONAUT_ENVIRONMENTS=develop`

###### アプリ起動時のシステムプロパティで設定する方法
アプリ起動時のシステムプロパティに下記を追加する

`java -Dmicronaut.environments=develop ....`