package sample.micronaut.api.handler;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.core.async.publisher.Publishers;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.security.handlers.ForbiddenRejectionUriProvider;
import io.micronaut.security.handlers.RedirectRejectionHandler;
import io.micronaut.security.handlers.RedirectRejectionHandlerConfiguration;
import io.micronaut.security.handlers.UnauthorizedRejectionUriProvider;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;

@Singleton
@Replaces(RedirectRejectionHandler.class)
public class CustomRejectionHandler extends RedirectRejectionHandler {

	/**
	 * Constructor.
	 *
	 * @param unauthorizedRejectionUriProvider      URI Provider to redirect to if unauthenticated
	 * @param forbiddenRejectionUriProvider         URI Provider to redirect to if authenticated but not enough authorization level.
	 * @param redirectRejectionHandlerConfiguration Redirect Rejection Handler Configuration
	 */
	public CustomRejectionHandler(UnauthorizedRejectionUriProvider unauthorizedRejectionUriProvider, ForbiddenRejectionUriProvider forbiddenRejectionUriProvider, RedirectRejectionHandlerConfiguration redirectRejectionHandlerConfiguration) {
		super(unauthorizedRejectionUriProvider, forbiddenRejectionUriProvider, redirectRejectionHandlerConfiguration);
	}

	@Override
	public Publisher<MutableHttpResponse<?>> reject(HttpRequest<?> request, boolean forbidden) {
		return Publishers.just(HttpResponse.status(forbidden ? HttpStatus.FORBIDDEN :
				HttpStatus.UNAUTHORIZED));
	}
}
