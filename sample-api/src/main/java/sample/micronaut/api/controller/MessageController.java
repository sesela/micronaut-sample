package sample.micronaut.api.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.reactivex.Maybe;

@Controller("/message")
@Secured({SecurityRule.IS_AUTHENTICATED, "ROLE_USER"})
public class MessageController {

    @Get("/{customerId}")
    public Maybe<Integer> messageList(@PathVariable("customerId") String customerId) {
        return Maybe.just(1);
    }
}