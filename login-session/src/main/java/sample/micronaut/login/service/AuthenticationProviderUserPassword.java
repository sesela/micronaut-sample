package sample.micronaut.login.service;

import io.micronaut.security.authentication.*;
import org.reactivestreams.Publisher;

import io.reactivex.Flowable;
import javax.inject.Singleton;
import java.util.Arrays;

@Singleton
public class AuthenticationProviderUserPassword implements AuthenticationProvider {

	@Override
	public Publisher<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {
		if (authenticationRequest.getIdentity().equals("sherlock") && authenticationRequest.getSecret().equals("password")) {
			UserDetails userDetails = new UserDetails((String) authenticationRequest.getIdentity(), Arrays.asList("ROLE_USER"));
			return Flowable.just(userDetails);
		}
		return Flowable.just(new AuthenticationFailed());
	}
}
