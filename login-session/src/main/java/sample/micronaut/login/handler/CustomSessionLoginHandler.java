package sample.micronaut.login.handler;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.security.authentication.AuthenticationFailed;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.session.SecuritySessionConfiguration;
import io.micronaut.security.session.SessionLoginHandler;
import io.micronaut.security.token.config.TokenConfiguration;
import io.micronaut.session.Session;
import io.micronaut.session.SessionStore;

import javax.inject.Singleton;
import java.net.URI;

@Requires(
		property = "micronaut.security.login-dynamic-redirect.enabled",
		value = "true"
)
@Singleton
@Replaces(SessionLoginHandler.class)
public class CustomSessionLoginHandler extends SessionLoginHandler {

	public CustomSessionLoginHandler(SecuritySessionConfiguration securitySessionConfiguration, SessionStore<Session> sessionStore, TokenConfiguration tokenConfiguration) {
		super(securitySessionConfiguration, sessionStore, tokenConfiguration);
	}

	public HttpResponse loginSuccess(UserDetails userDetails, HttpRequest<?> request, String url) {
		super.loginSuccess(userDetails, request);
		// TODO　urlバリデーションを追加する
		// xxxxxx
		if (url == null || url.isEmpty()) {
			url = securitySessionConfiguration.getLoginSuccessTargetUrl();
		}
		return HttpResponse.seeOther(UriBuilder.of(url).build());
	}

	public HttpResponse loginFailed(AuthenticationFailed authenticationFailed, String key, String url) {
		if (url == null || url.isEmpty()) {
			url = securitySessionConfiguration.getLoginSuccessTargetUrl();
		}
		URI location = UriBuilder.of(securitySessionConfiguration.getLoginFailureTargetUrl()).queryParam(key, url).build();
		return HttpResponse.seeOther(location);
	}

}
