package sample.micronaut.login.controller;

import sample.micronaut.login.handler.CustomSessionLoginHandler;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.event.ApplicationEventPublisher;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.annotation.Error;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.*;
import io.micronaut.security.endpoints.LoginController;
import io.micronaut.security.event.LoginFailedEvent;
import io.micronaut.security.event.LoginSuccessfulEvent;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.micronaut.views.View;
import io.reactivex.Flowable;
import io.reactivex.Single;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.*;


@Requires(
		property = "micronaut.security.endpoints.login.enabled",
		value = "true"
)
@Requires(beans = CustomSessionLoginHandler.class)
@Controller("${micronaut.security.endpoints.login.path:/login}")
@Secured(SecurityRule.IS_ANONYMOUS)
@Replaces(LoginController.class)
@Validated
public class CustomLoginController {

	protected static final String REDIRECT_PARAM_NAME = "url";

	protected final Authenticator authenticator;
	protected final CustomSessionLoginHandler loginHandler;
	protected final ApplicationEventPublisher eventPublisher;

	public CustomLoginController(Authenticator authenticator, CustomSessionLoginHandler loginHandler, ApplicationEventPublisher eventPublisher) {
		this.authenticator = authenticator;
		this.loginHandler = loginHandler;
		this.eventPublisher = eventPublisher;
	}


	@Get
	@View("auth")
	public Map<String, Object> auth(@QueryValue(value = REDIRECT_PARAM_NAME, defaultValue = "") String url) {
		Map<String, Object> map = new HashMap<>();
		map.put("key", REDIRECT_PARAM_NAME);
		map.put("url", url);
		return map;
	}


	@Get("/authFailed")
	@View("auth")
	public Map<String, Object> authFailed(@QueryValue(value = REDIRECT_PARAM_NAME, defaultValue = "") String url) {
		Map<String, Object> map = new HashMap<>();
		map.put("errors", true);
		map.put("key", REDIRECT_PARAM_NAME);
		map.put("url", url);
		return map;
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON})
	@Post
	public Single<HttpResponse> login(@Valid UsernamePasswordCredentials usernamePasswordCredentials, HttpRequest<?> request, @QueryValue(value = REDIRECT_PARAM_NAME, defaultValue = "") String url) {
		Flowable<AuthenticationResponse> authenticationResponseFlowable = Flowable.fromPublisher(authenticator.authenticate(usernamePasswordCredentials));
		return authenticationResponseFlowable.map(authenticationResponse -> {
			if (authenticationResponse.isAuthenticated()) {
				UserDetails userDetails = (UserDetails) authenticationResponse;
				eventPublisher.publishEvent(new LoginSuccessfulEvent(userDetails));
				return loginHandler.loginSuccess(userDetails, request, url);
			}
			AuthenticationFailed authenticationFailed = (AuthenticationFailed) authenticationResponse;
			eventPublisher.publishEvent(new LoginFailedEvent(authenticationFailed));
			return loginHandler.loginFailed(authenticationFailed, REDIRECT_PARAM_NAME, url);
		}).first(HttpResponse.status(HttpStatus.UNAUTHORIZED));
	}

	@Error(exception = ConstraintViolationException.class)
	public HttpResponse validationError(HttpRequest request, ConstraintViolationException ex, @QueryValue(value = REDIRECT_PARAM_NAME, defaultValue = "") String url) {
		return loginHandler.loginFailed(null, REDIRECT_PARAM_NAME, url);
	}

}
